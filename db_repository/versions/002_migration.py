from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
comment = Table('comment', post_meta,
    Column('Comment_ID', Integer, primary_key=True),
    Column('Body', Text),
    Column('UserIdentity', Integer),
    Column('User_ID', String(length=7)),
    Column('UserName', String(length=16)),
    Column('Timestamp', DateTime),
    Column('Post_ID', Integer),
    Column('Topic', Text),
    Column('Replied_ID', Integer),
)

course = Table('course', post_meta,
    Column('Course_ID', String(length=8), primary_key=True, nullable=False),
    Column('Course_Name', String(length=32), nullable=False),
    Column('Teacher_ID', String(length=7)),
)

post = Table('post', post_meta,
    Column('Post_ID', Integer, primary_key=True),
    Column('Course', String(length=32)),
    Column('Topic', Text),
    Column('Body', Text),
    Column('UserIdentity', Integer),
    Column('User_ID', String(length=7)),
    Column('UserName', String(length=16)),
    Column('Timestamp', DateTime),
    Column('IsPosted', Boolean),
)

problem = Table('problem', post_meta,
    Column('Problem_ID', String(length=5), primary_key=True, nullable=False),
    Column('Problem_Name', String(length=20), nullable=False),
    Column('Time_Limit', Integer, default=ColumnDefault(1)),
    Column('Memory_Limit', Integer, default=ColumnDefault(128)),
)

record = Table('record', post_meta,
    Column('Record_ID', String(length=16), primary_key=True, nullable=False),
    Column('Student_ID', String(length=7)),
    Column('Problem_ID', String(length=5)),
    Column('Status', String(length=3), nullable=False),
    Column('Submit_Time', DateTime),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['comment'].create()
    post_meta.tables['course'].create()
    post_meta.tables['post'].create()
    post_meta.tables['problem'].create()
    post_meta.tables['record'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['comment'].drop()
    post_meta.tables['course'].drop()
    post_meta.tables['post'].drop()
    post_meta.tables['problem'].drop()
    post_meta.tables['record'].drop()
