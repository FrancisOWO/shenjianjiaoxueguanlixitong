from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
comment = Table('comment', post_meta,
    Column('Comment_ID', Integer, primary_key=True),
    Column('Body', Text),
    Column('UserIdentity', Integer),
    Column('User_ID', String(length=7)),
    Column('UserName', String(length=16)),
    Column('Timestamp', DateTime),
    Column('Post_ID', Integer),
    Column('Topic', Text),
    Column('Position', Integer),
    Column('Replied_ID', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['comment'].columns['Position'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['comment'].columns['Position'].drop()
