import unittest
from app import app, db
from app.models import Student, Teacher, Admin
from flask_login import current_user
import os


class Test_Case(unittest.TestCase):

    def setUp(self):
        app.config.update(
            TESTING=True,
            WTF_CSRF_ENABLED=False,
            SQLALCHEMY_DATABASE_URI='sqlite:///:memory:'
        )

        db.create_all()
        self.client = app.test_client()
        # 测试管理员账号
        user = Admin(Admin_ID='Admin',
                     AdminName='Admin',
                     Password=996
                     )
        db.session.add(user)
        db.session.commit()

        # 测试教师账号
        response = self.register(
            userid='12345',
            username='test_tea',
            password1='test_password123',
            password2='test_password123',
            identification='Teacher',
            email='114514@qq.com',
            phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("注册成功", data)

        # 测试学生账号
        response = self.register(
            userid='1234567',
            username='test_stu',
            password1='test_password123',
            password2='test_password123',
            identification='Student',
            email='114514@qq.com',
            phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("注册成功", data)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def register(self, userid, username, password1, password2, identification, email, phone=None):
        return self.client.post('/register', data={
                                'userid': userid,
                                'username': username,
                                'password1': password1,
                                'password2': password2,
                                'identification': identification,
                                'email': email,
                                'phone': phone
                            }, follow_redirects=True
            )

    def test_app_exist(self):
        self.assertFalse(app is None)

    def test_app_is_testing(self):
        self.assertTrue(app.config['TESTING'])

    def test_index_page(self):
        response = self.client.get('/')
        data = response.get_data(as_text=True)
        self.assertTrue('神剑教学管理系统 - 主页' in data)

    def logout(self):
        self.client.get('/logout')

    def login(self, identification=None, userid=None, password=None):
        # 三无默认登录学生
        if userid is None and password is None and identification is None:
            user = Student.query.first()
            userid = user.get_id()
            password = user.Password
        # 仅有identification则按其登录
        if userid is None and password is None:
            if identification == 'Student':
                user = Student.query.first()
            elif identification == 'Teacher':
                user = Teacher.query.first()
            elif identification == 'Admin':
                user = Admin.query.first()

            userid = user.get_id()
            password = user.Password

        return self.client.post('/login',
                                data={
                                    'identification': identification,
                                    'userid': userid,
                                    'password': password
                                },
                                follow_redirects=True
                                )

    def test_login_page(self):

        # 失败
        # case 1:账号密码都不存在
        response = self.login('Student', '123456578', '123124')
        data = response.get_data(as_text=True)
        self.assertNotIn("成功", data)
        # case 2:账号不存在，密码存在
        response = self.login('Student', '123456578', '123')
        data = response.get_data(as_text=True)
        self.assertNotIn("成功", data)
        # case 3:账号存在，密码不匹配
        response = self.login('Student', '222', '1234')
        data = response.get_data(as_text=True)
        self.assertNotIn("成功", data)

        # 成功
        # case 1: 学生成功
        response = self.login(identification='Student')
        data = response.get_data(as_text=True)
        self.assertIn("成功", data)
        self.client.post('/logout')

        self.logout()
        # case 2: 教师成功
        response = self.login(identification='Teacher')
        data = response.get_data(as_text=True)
        self.assertIn("成功", data)
        self.client.post('/logout')
        self.logout()

        # case 3: 管理员成功
        response = self.login(identification='Admin')
        data = response.get_data(as_text=True)
        self.assertIn("成功", data)
        self.client.post('/logout')
        self.logout()

    def test_register_page(self):
        '''
        注册要求：  学生id长度为7、教师id长度为5，不含中文
                    密码长度8-16，含有字母与数字

        '''
        # 注册成功的case已在setUp实现，不再复测

        # case 1: 学生id长度不为7
        response = self.register(userid='123456',
                                 username='test',
                                 password1='test_password123',
                                 password2='test_password123',
                                 identification='Student',
                                 email='114514@qq.com',
                                 phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("学生ID长度应为7", data)

        # case 2: 教师id长度不为5
        response = self.register(userid='123456',
                                 username='test',
                                 password1='test_password123',
                                 password2='test_password123',
                                 identification='Teacher',
                                 email='114514@qq.com',
                                 phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("教师ID长度应为5", data)

        # case 3: 教师id长度不为5
        response = self.register(userid='123456',
                                 username='test',
                                 password1='test_password123',
                                 password2='test_password123',
                                 identification='Teacher',
                                 email='114514@qq.com',
                                 phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("教师ID长度应为5", data)

        # case 4: id含中文
        response = self.register(userid='用户id',
                                 username='test',
                                 password1='test_password123',
                                 password2='test_password123',
                                 identification='Teacher',
                                 email='114514@qq.com',
                                 phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("用户ID不应包含中文", data)

        # case 5: 密码长度小于8
        response = self.register(userid='12345',
                                 username='test',
                                 password1='a12345',
                                 password2='a12345',
                                 identification='Teacher',
                                 email='114514@qq.com',
                                 phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("密码必须是8-16位", data)

        # case 6: 密码长度大于16
        response = self.register(userid='12345',
                                 username='test',
                                 password1='a1234567891234567',
                                 password2='a1234567891234567',
                                 identification='Teacher',
                                 email='114514@qq.com',
                                 phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("密码必须是8-16位", data)

        # case 7: 密码不含数字
        response = self.register(userid='12345',
                                 username='test',
                                 password1='abcdefghijkl',
                                 password2='abcdefghijkl',
                                 identification='Teacher',
                                 email='114514@qq.com',
                                 phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("必须含数字与字母", data)

        # case 8: 密码不含字母
        response = self.register(userid='12345',
                                 username='test',
                                 password1='123456789',
                                 password2='123456789',
                                 identification='Teacher',
                                 email='114514@qq.com',
                                 phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("必须含数字与字母", data)

        # case 9: 已有ID
        user = Student.query.first()
        response = self.register(userid=user.get_id(),
                                 username='test',
                                 password1=user.Password,
                                 password2=user.Password,
                                 identification='Student',
                                 email='114514@qq.com',
                                 phone='1919910')
        data = response.get_data(as_text=True)
        self.assertIn("账号已注册", data)


if __name__ == '__main__':
    unittest.main()
