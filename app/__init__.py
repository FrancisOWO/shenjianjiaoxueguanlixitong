import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_openid import OpenID
from flask_bootstrap import Bootstrap
from config import Config, basedir

app = Flask(__name__)
app.config.from_object(Config)
app.config.setdefault('BOOTSTRAP_SERVE_LOCAL', True)    #从本地加载bootstrap
db = SQLAlchemy(app)
db.create_all()
migrate = Migrate(app, db)
login = LoginManager(app)
login.init_app(app)
login.login_view = 'login'
bootstrap = Bootstrap(app)

# 后缀-命令
SuffixCommand = {"C":"gcc","C++":"g++","Python":"python"}
# 后缀-语言
SuffixLanguage = {"c":"C","cpp":"C++","py":"Python","all":None}
# Problem-<Problem_ID>下的目录
Problem_Details_Class = ['Background','Description', 'InputFormat', 'OutputFormat', 'SampleInput', 'SampleOutput']
# Judging Result
JudgeResult = {'all': '全部',
            'AC': '正确',
            'WA': '错误',
            'TLE': '超时 ',
            'OLE': '超出输出限制',
            'MLE': '超出内存限制',
            'RE': '运行时错误',
            'PE': '格式错误',
            'CE': '编译错误',
            'IC': '非法代码',
            'SE': '系统错误',
            'WAITING':'等待中',
            'JUDGING':'判题中'}

from app import views, models
from app import commands