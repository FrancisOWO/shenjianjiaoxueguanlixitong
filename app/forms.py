from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, RadioField, SelectField, TextAreaField, IntegerField, FileField, MultipleFileField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Optional, NumberRange, Length, Regexp
from app.models import Student
from flask_wtf.file import FileRequired
import re
def contain_digit_and_alpha(form,field) :
    string=field.data
    pattern= re.compile('[a-zA-Z0-9]')
    
    match = pattern.findall(string)
  
    if (not match) or string.isdigit() or string.isalpha():
        raise ValidationError('必须含数字与字母')
   

def all_digit(form,field) :
    string=field.data
    if not string.isdigit() :
        raise ValidationError('必须全为数字')
    
    
  
    

class LoginForm(FlaskForm):
    userid = StringField(
        '学生 / 老师ID',
        validators=[DataRequired()],
        render_kw={
            'class':'form-control'
        }
    )
    password = PasswordField(
        '密码',
        validators=[DataRequired()],
        render_kw={
            'class':'form-control'
        }
    )
    identification = SelectField(
        '身份',
        validators=[DataRequired()],
        choices=[
            ('Student', '学生'),
            ('Teacher', '老师'),
            ('Admin', '管理员')
        ],
        default='Student',
        render_kw={
            'class':'wide'
        }
    )
    remember_me = BooleanField('记住我')
    submit = SubmitField('登录',
                         render_kw={
                             'class': 'd-block py-3 px-5 bg-primary text-white border-0 rounded font-weight-bold mt-3 center p-2'
                         })


class RegisterForm(FlaskForm):
    userid = StringField(
        '学生 / 老师ID',
        validators=[DataRequired(message="请输入ID!"),
                    all_digit],
        render_kw={
            'class': 'form-control',
            'placeholder': 'ID长度：学生7位数字、教师5位数字'
        }
    )
    username = StringField(
        '用户名',
        validators=[DataRequired(message="请输入用户名!")],
        render_kw={
            'class': 'form-control'
        }
    )
    password1 = PasswordField(
        '密码',
        validators=[
            DataRequired(message="请输入密码!"),
            Length(min = 8, max = 16, message="密码必须是8-16位"),
            contain_digit_and_alpha
            ],
        render_kw={
            'class': 'form-control',
            'placeholder':'密码长度8-16位，必须含数字与字母'
        }
    )
    password2 = PasswordField(
        '重复密码',
        validators=[DataRequired(), 
                    EqualTo('password1', message="两次输入的密码不一致!")
                    ],
        render_kw={
            'class': 'form-control'
        }
    )
    identification = SelectField(
        '身份',
        validators=[DataRequired()],
        choices=[
            ('Student', '学生'),
            ('Teacher', '老师')
        ],
        default='Student',
        render_kw={
            'class': 'wide'
        }
    )
    email = StringField(
        '邮箱',
        validators=[
            DataRequired(), 
            Email(message="非法邮箱格式!")],
        render_kw={
            'class': 'form-control'
        })
    phone = StringField('手机（可选）',
                        validators=[Optional()],
                        render_kw={
                            'class': 'form-control'
                        })
    submit = SubmitField('注册',
                         render_kw={
                             'class': 'd-block py-3 px-5 bg-primary text-white border-0 rounded font-weight-bold mt-3 center'
                         })


class AddPostForm(FlaskForm):
    courseID = SelectField('课程',
                             validators=[DataRequired(message = "未选择课程")],
                             choices=[
                                 ('高程', '高程'),
                                 ('软工', '软工'),
                                 ('算法', '算法'),
                                 ('计组', '计组'),
                                 ('自动机', '自动机'),
                                 ('其他', '其他')
                             ],
                             default='高程',
                             render_kw={
                                 'class': 'wide'
                             }) 
    topic = StringField('主题',
                          validators=[DataRequired(message = "未输入主题"),
                                     Length(min=1,max=40,message="长度应为1-40个字符")],
                          render_kw={
                              'placeholder':'在这里输入主题，不得超过40个字符',
                              'class': 'border w-100 p-2 bg-white',
                              'maxlength':'40'
                          })
    post = TextAreaField('内容',
                         validators=[DataRequired(message = "未输入内容"),
                                    Length(min=1,max=2000,message="长度应为1-2000个字符")],
                         render_kw={
                             "rows": "10",
                             'placeholder':'在这里输入内容，不得超过2000个字符',
                             'class': 'border w-100 p-2',
                             'maxlength':'2000'
                         })
    submit = SubmitField('发起讨论',
                         render_kw={
                             'class': 'center btn btn-primary d-block mt-2 px-5'
                         })
    save = SubmitField('保存讨论',
                         render_kw={
                             'class': 'center btn btn-primary d-block mt-2 px-5'
                         })

class EditDeleteForm(FlaskForm):
    submitEdit = SubmitField('编辑',
                         render_kw={
                             'class': 'center btn btn-primary d-block mt-2 px-3'
                         })
    submitDelete = SubmitField('删除',
                         render_kw={
                             'class': 'center btn btn-primary d-block mt-2 px-3'
                         })

class ReplyForm(FlaskForm):
    submitReply = SubmitField('回复',
                         render_kw={
                             'class': 'center btn btn-primary d-block mt-2 px-3'
                         })


class SubmitForm(FlaskForm):
    compiler = SelectField(
        '编译器',
        validators=[DataRequired()],
        choices=[
            ('C', 'gcc'),
            ('C++', 'g++'),
            ('Python','python3')
        ],
        default='MSVC'
    )
    code = TextAreaField('代码内容',
                         render_kw={
                             "rows": "10",
                             'placeholder':'在这里输入代码',
                             'class': 'border w-100 p-2'
                         })
    
    uploadfile = MultipleFileField(
        '上传代码'
    )

    submit = SubmitField('Submit',
                         render_kw={
                             'class': 'center btn btn-primary d-block mt-2 px-5'
                         })



class SubmitResultForm(FlaskForm):
    problemId = StringField(
        '题目编号',
        validators=[Optional()] #[Optional(), NumberRange(1, 9999, '题目id错误!')]
    )
    language = SelectField(
        '语言',
        validators=[DataRequired()],
        choices=[
            ('all', '全部'),
            ('cpp', 'C++'),
            ('py', 'Python'),
            ('c', 'C')
        ],
        default='all'
    )
    result = SelectField(
        '运行结果',
        validators=[DataRequired()],
        choices=[
            ('all', '全部'),
            ('AC', '正确'),
            ('WA', '错误'),
            ('TLE', '超时 '),
            ('OLE', '超出输出限制'),
            ('MLE', '超出内存限制'),
            ('RE', '运行时错误'),
            ('PE', '格式错误'),
            ('CE', '编译错误'),
            ('IC', '非法代码'),
            ('SE', '系统错误'),
            ('WAITING','等待中'),
            ('JUDGING','判题中')
        ],
        default='all'
    )
    submitFilter = SubmitField('确定')

class MyInfoEditForm(FlaskForm):
    username = StringField(
        '用户名',
        validators=[Optional()],
        render_kw={
            'class': 'form-control'
        }
    )
    profilephoto = FileField(
        '头像',
        validators=[Optional()]
    )
    submit = SubmitField('保存更改',
                         render_kw={
                             'class': 'btn btn-transparent'
                         })


class AccountCheckForm(FlaskForm):
    userid = StringField(
        '学生ID',
        validators=[DataRequired(message="请输入ID!")],
        render_kw={
            'class': 'form-control'
        }
    )
    email = StringField(
        '邮箱',
        validators=[Email(message="非法邮箱格式!")],
        render_kw={
            'class': 'form-control'
        })
    phone = StringField('手机（可选）',
                        validators=[Optional()],
                        render_kw={
                            'class': 'form-control'
                        })
    submit = SubmitField('验证',
                         render_kw={
                             'class': 'd-block py-3 px-5 bg-primary text-white border-0 rounded font-weight-bold mt-3 center'
                         })

class AccountNoPasswordResetForm(FlaskForm):
    password1 = PasswordField(
        '新密码',
        validators=[DataRequired(message="请输入密码!"),
                    Length(min = 8, max = 16, message="密码必须是8-16位"),
                    contain_digit_and_alpha],
        render_kw={
            'class': 'form-control',
            'placeholder': '密码长度8-16位，必须含数字与字母'
        }
    )
    password2 = PasswordField(
        '重复新密码',
        validators=[DataRequired(), EqualTo(
            'password1', message="两次输入的密码不一致!")],
        render_kw={
            'class': 'form-control'
        }
    )    
    submit = SubmitField('重置密码',
                         render_kw={
                             'class': 'd-block py-3 px-5 bg-primary text-white border-0 rounded font-weight-bold mt-3 center'
                         })

class AccountHasPasswordResetForm(FlaskForm):
    password_old = PasswordField(
        '旧密码',
        validators=[DataRequired(message="请输入密码!")],
        render_kw={
            'class': 'form-control'
        }
    )
    password1 = PasswordField(
        '新密码',
        validators=[DataRequired(message="请输入密码!"),
                    Length(min = 8, max = 16, message="密码必须是8-16位"),
                    contain_digit_and_alpha],
        render_kw={
            'class': 'form-control',
            'placeholder':'密码长度8-16位，必须含数字与字母'
        }
    )
    password2 = PasswordField(
        '重复新密码',
        validators=[DataRequired(), EqualTo(
            'password1', message="两次输入的密码不一致!")],
        render_kw={
            'class': 'form-control'
        }
    )    
    submit = SubmitField('修改密码',
                         render_kw={
                             'class': 'btn btn-transparent'
                         })

class AccountAppealForm(FlaskForm):
    title = StringField('主题概要',
                        validators=[DataRequired()],
                        render_kw={
                            'class': 'border w-100 p-2 bg-white text-capitalize'
                        })
    reason = TextAreaField('申诉原因',
                           validators=[DataRequired()],
                           render_kw={
                               "rows": "10",
                               'class': 'border p-3 w-100'
                           })
    submit = SubmitField('提交申诉',
                         render_kw={
                             'class': 'd-block py-3 px-5 bg-primary text-white border-0 rounded font-weight-bold mt-3 center'
                         })

class AddCommentForm(FlaskForm):
    comment = TextAreaField(
        '添加评论',
        validators=[DataRequired(message="未输入内容"),
                Length(min=1,max=600,message="长度应为1-600个字符")],
         render_kw={
             "rows": "5",
             'placeholder': '在这里输入内容，不得超过600个字符',
             'maxlength':'600'
         }
    )
    submitComment = SubmitField('发表评论')

class AddProblemForm(FlaskForm):
    problemName = StringField(
        '题目名称',
        validators=[
            DataRequired(message='请输入题目名称')
        ]
    )
    problemBackground = TextAreaField(
        '题目背景',
        validators=[
            DataRequired(message='请输入题目背景')
        ]
    )
    problemDescription = TextAreaField(
        '题目描述',
        validators=[
            DataRequired(message='请输入题目描述')
        ]
    )
    problemInput = TextAreaField(
        '题目输入',
        validators=[
            DataRequired(message='请输入题目输入')
        ]
    )
    problemOutput = TextAreaField(
        '题目输出',
        validators=[
            DataRequired(message='请输入题目输出')
        ]
    )
    exampleInput = TextAreaField(
        '样例输入',
        validators=[
            DataRequired(message='请输入样例输入')
        ]
    )
    exampleOutput = TextAreaField(
        '样例输出',
        validators=[
            DataRequired(message='请输入样例输出')
        ]
    )
    timeLimitation = IntegerField(
        '时间限制',
        validators=[
            NumberRange(1,10,'合法范围1~10ms')
        ]
    )
    memoryLimitation = IntegerField(
        '内存限制',
        validators=[
            NumberRange(1,1024,'合法范围1~1024MB')
        ]
    )
    inputdata = MultipleFileField(
        '输入数据文件',
        validators=[
            DataRequired(message='请上传至少一个输入数据文件')
        ]
    )
    outputdata = MultipleFileField(
        '输出数据文件',
        validators=[
            DataRequired(message='请上传至少一个输出数据文件')
        ]
    )
    submit = SubmitField('确认上传')

class AddNoticeForm(FlaskForm):
    courseID = SelectField('课程',
                             validators=[DataRequired(message = "未选择课程")],
                             choices=[
                                 ('高程', '高程'),
                                 ('软工', '软工'),
                                 ('算法', '算法'),
                                 ('计组', '计组'),
                                 ('自动机', '自动机'),
                                 ('其他', '其他')
                             ],
                             default='高程',
                             render_kw={
                                 'class': 'wide'
                             }) 
    topic = StringField('标题',
                          validators=[DataRequired(message = "未输入标题"),
                                    Length(min=1,max=40,message="长度应为1-40个字符")],
                          render_kw={
                              'placeholder':'在这里输入标题，不得超过40个字符',
                              'class': 'border w-100 p-2 bg-white',
                              'maxlength':'40'
                          })
    post = TextAreaField('内容',
                         validators=[DataRequired(message = "未输入内容"),
                                    Length(min=1,max=600,message="长度应为1-600个字符")],
                         render_kw={
                             "rows": "10",
                             'placeholder':'在这里输入内容，不得超过600个字符',
                             'class': 'border w-100 p-2',
                             'maxlength':'600'
                         })
    submit = SubmitField('确认发布',
                         render_kw={
                             'class': 'center btn btn-primary d-block mt-2 px-5'
                         })

#这个EditNotificationForm好像没用了
class EditNotificationForm(FlaskForm):
    title = StringField(
        '通知标题',
        validators=[
            DataRequired(message='请输入通知标题')
        ]
    )
    content = TextAreaField(
        '通知正文',
        validators=[
            DataRequired(message='请输入通知正文')
        ]
    )
    submit = SubmitField('确认发布')