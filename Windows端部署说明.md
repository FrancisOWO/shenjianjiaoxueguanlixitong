# Windows端本地部署项目

## 操作步骤

1. 将项目文件下载至本地
2. 安装g++（使用MinGW进行安装,安装步骤见下）
3. 安装python3.7（建议使用anaconda）
4. 进入cmd窗口
   - 切换盘符
   - ```cd 此处替换你存放下载的项目文件所在路径```进入文件夹
   - 输入```pip install -i https://pypi.doubanio.com/simple/ -r requirements.txt```安装依赖库
   - 输入```python app.py```即可运行项目
   - 在浏览器中键入地址```127.0.0.1：5000```即可进入网站

## MinGW 安装指南

编写日期：2020/6/12

一、  MinGW 的下载与安装

1. 打开 MinGW 官网<http://www.mingw.org/>，点击上方 Downloads，进入下载界面。下拉页面，找到安装包下载地址，点击进入下载。（建议存放安装包文件夹路径全部为英文路径）
2. 点击安装包进行安装,选择 Install 进行安装选择安装路径，建议全英文路径；选择 Continue
等待下载，具体时间决定于网速。 待下载完成后，选择 Continue 继续。安装完成，会自动弹出 MinGW 窗口，如果没有自动弹出，也可在桌面找到 MinGW Installer，图标双击打开。

二、  安装 G++/GCC 编译器

1. 在 MinGW→Basic Setup 页面中，选中 mingw32-gcc-g++-bin 点击右键，点击 Mark for Installation。
2. 在 Installation 中选中 Apply Changes，选择 Apply 继续。 待安装成功后，按 Close 关闭该界面。
3. 添加 g++路径到环境变量，找到 MinGW 的安装路径，E.g. F:\MinGW，进入 bin 文件夹，然后将路径复制。接着右键我的电脑选择属性，在左侧菜单栏中选择高级系统设置，选择环境变量。在系统变量中找到 Path，选中后点击编辑，在编辑环境变量窗口中点击新建，将 先前复制的路径复制到新的环境变量条目中，点击确定退出。

三、  测试成功安装 G++/GCC

按组合键：Windows 键+R 键  打开运行窗口，输入 cmd 点击确定打开 cmd 命令行窗口。在 cmd 窗口中输入 ```g++ --version``` 查看 g++版本信息，若输出g++版本则表示 g++正常安装。
